import sys, traceback
sys.path.append("..")
from vedavaapi.ashtadhyayi.cmdline import *
from pprint import pprint, pformat
from examples.rupa_siddhi_inputs import *
import inspect
import csv

# Expected transformation

def annotate(pada_descs, samjna_desc):
    for p in pada_descs:
        if samjna_desc:
            sutra_id = samjna_desc['sutra_id'] if 'sutra_id' in samjna_desc else None
            a().label_samjna(p, samjna_desc['samjna'], sutra_id=sutra_id)
        else:
            a().label_samjnas(p)
        pprint(p)

#if (len(sys.argv) < 2):
    #usage()

def analyze(pada, inscr="SLP1", outscr="SLP1"):
    script(outscr)

    # Find the sutra sequence to execute for given pada sequence
    for alg in rupa_siddhi_inputs:
        if pada == alg['result']:
            return ppformat(alg, outscr=outscr)

    return None

@logged_func
def interpret(padas, sutras=[], result=None, inscr="SLP1", outscr="SLP1"):
    script(outscr)
    transforms = []
    print(padas)
    algo = {'padas': xliterate_obj(padas, inscr, "SLP1"),
            'result': xliterate(result, inscr, "SLP1"), 'sutras': sutras}
    info("Using algo: ", algo)
    try:
        res = PadaSequence(padas=algo['padas'])
        res2, transforms = a().apply_vidhi_rules(res, sutras=algo['sutras'])
        if res2:
            res = res2
        else:
            log(f"Error: No vidhi sutras matched.")
        if res:
            form = "".join(res.varnas())
            log("Finally, {} = {}".format(ppformat(padas), ppformat(form)))
            success = (not algo['result'] or form == algo['result'])
            if not success:
                log("Failed.")
        return { 'padas': xliterate_obj(algo['padas'], "SLP1", outscr),
                 'generated': xliterate(form, "SLP1", outscr),
                 'expected': xliterate(algo['result'], "SLP1", outscr),
                 'mods': transforms,
                 'success': success,
        }
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        info("Failed: Caught exception", e)
    return None

if __name__ == '__main__':
    pgmname = sys.argv.pop(0)

    def usage():
        print("Usage: " + pgmname + " [-v <loglevel>] [-r] <samjna_pada> <pada_1> [<pada2> ...]")
        print("    -r   Rebuild Ashtadhyayi database")
        print("    -v <loglevel>"
              "         Log messages at <loglevel> verbosity.")
        print("    -l <SCRIPT>"
              "         Produce output in <SCRIPT>."
              "         <SCRIPT> can be SLP1, DEVANAGARI, IAST, TELUGU etc."
              "         Default script is SLP1.")
        print("    -s <sutra_id>[,<sutra_id>]"
              "         Apply given sutras in sequence."
              "         Default: Auto-select sutras.")
        print("    -e <expected_result>"
              "         Expect given result.")
        exit(1)

    rebuild = False
    mock = False
    exp_result = None
    infile = None
    inpadas = []
    sutras = []
    outscript = "SLP1"
    set_req_context(ReqContext())
    while sys.argv:
        if sys.argv[0] == '-v':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            loglevel(sys.argv[0])
        elif sys.argv[0] == '-r':
            rebuild = True
        elif sys.argv[0] == '-h':
            usage()
        elif sys.argv[0] == '-n':
            mock = True
        elif sys.argv[0] == '-e':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            exp_result = sys.argv[0]
        elif sys.argv[0] == '-l':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            outscript = sys.argv[0]
        elif sys.argv[0] == '-f':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            infile = sys.argv[0]
        elif sys.argv[0] == '-s':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            sutras = sys.argv[0].split(',')
            print(sutras)
        else:
            inpadas = sys.argv
            break
        sys.argv.pop(0)

    try:

        ashtadhyayi(rebuild)

        if infile:
            stats = {'ntotal': 0, 'nsuccess': 0, 'ncrash': 0, 'nskipped': 0}
            for row in read_csvfile(infile):
                set_req_context(ReqContext())
                padas = f"{row['upasarga']} {row['dhatu']} {row['pratyaya']}"
                stats['ntotal'] += 1
                if int(row['loglevel']) < 0:
                    stats['nskipped'] += 1
                    log(f"skipping {padas}")
                    continue
                print(row)
                loglevel(row['loglevel'])
                padas = padas.split()
                sutras = re.split(r'\s*,\s*', row['sutras'].replace('\s', ''))
                result = interpret(padas, inscr=row['inscript'],
                          outscr=row['outscript'], sutras=[],
                          result=row['exp_result'])
                if result:
#                    result['mods'] = None
                    info(f"{pformat(result)}")
                    if result['success']:
                        stats['nsuccess'] += 1
                else:
                    stats['ncrash'] += 1

                info(stats)
                with open(f"output-{stats['ntotal']}.json", "w") as outf:
                    outf.write(print_dict(r()) + "\n")

                if not result or not result['success']:
                    pass
#                  break
        elif inpadas:
            log(f"Analyzing {ppformat(inpadas)} with sutras {sutras} ...")
            result = interpret(inpadas, inscr="SLP1", sutras=sutras, result=exp_result, outscr=outscript)
            if result:
                info(f"{pformat(result)}")
        else:
            for algo in rupa_siddhi_inputs:
                padas = algo['padas'].split()
                log(f"Analyzing {ppformat(padas)} with sutras {sutras} ...")
                input("Press enter to continue: ...")
                if not sutras:
                    sutras = algo['sutras']
                result = interpret(padas, sutras=sutras, result=algo['result'], \
                                   outscr=outscript)
                if result:
                    info(f"{pformat(result)}")
                if inpadas:
                    break
                continue
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        log("Failed: Caught exception", e)

    sys.exit(0)