rupa_siddhi_inputs = [
    {
        'padas': 'gamx~ Satf~',
        'sutras': [
            34067, # kartari krt
            31068, # kartari shap
            73077, # ishu gami yamaaM chhaH
            61073, # chhe cha
            61097, # ato guNe
            84040, # stoH schunA schuH
        ],
        'result': 'gacCat'
    },
    {
        'padas': 'paWa~ Satf~',
        'sutras': [
            34067,  # kartari krt
            31068,  # kartari shap
            61097,  # ato guNe
        ],
        'result': 'paWat'
    },
	{
        'padas': 'aki~ anIyar',
        'sutras': [
            71058,  # idito num dhAtoH
            83024,  # nashchApadAntasya jhali
            84058,  # anusvArasya yayi parasavarNaH
        ],
        'result': 'aNkanIya'
    },
	{
        'padas': 'fDu~ kyap',
        'sutras': [
            11005,  # kkNGiti ca
        ],
        'result': 'fDya'
    },
	{
        'padas': 'iN tavya',
        'sutras': [
			73084,	# sArvadhAtukArdhadhAtukayoH
        ],
        'result': 'etavya'
    },
	{
        'padas': 'kadi~ tavyat',
        'sutras': [
            71058,  # idito num dhAtoH
			72035,	# ArdhadhAtukasyeD valAdeH
            83024,  # nashchApadAntasya jhali
            84058,  # anusvArasya yayi parasavarNaH
        ],
        'result': 'kanditavya'
    },
	{
        'padas': 'uN yat',
        'sutras': [
            73084,	# sArvadhAtukArdhadhAtukayoH
            61080,  # dhatostannimittasyaiva
        ],
        'result': 'avya'
    },
#	{
#        'padas': 'Cidir kelimar',
#        'sutras': [
#            11005,  # kkNGiti ca
#        ],
#        'result': 'Cidelima'
#    },
	{
        'padas': 'daridrA Rvul',
        'sutras': [
            71001,  # yuvoranAkau
            73033,  # Ato yuk chiNkRitoH
        ],
        'result': 'daridrAyaka'
    },
	{
        'padas': 'kramu~ tfc',
        'sutras': [
            72035,  # ArdhadhAtukasyeD valAdeH
        ],
        'result': 'kramitf'
    },
	{
        'padas': 'vadi~ SAnac',
        'sutras': [
            71058,  # idito num dhAtoH
            34067,  # kartari krt
            31068,	# kartari shap
#			61101,	# akaH savarNe dIrghaH
			72082,	# Ane muk
			83024,	# nashchApadAntasya jhali
			84058,	# anusvArasya yayi parasavarNaH
        ],
        'result': 'vandamAna'
    },
	{
        'padas': 'Ruda~ lyuw',
        'sutras': [
            61065,  # No naH
			71001,  # yuvoranAkau
			73086,	# pugantasyalaghUpadhasya cha
        ],
        'result': 'nodana'
    },
	{
        'padas': 'YimidA~ kta',
        'sutras': [
			72035,  # ArdhadhAtukasyeD valAdeH
			73086,	# pugantasyalaghUpadhasya cha
        ],
        'result': 'medita'
    },
	{
        'padas': 'mAN ktavatu',
        'sutras': [
			72010,  # ekAcha upadeshe anudAttAt
			74040,	# dyatisyatimAsthAmitti kiti 
        ],
        'result': 'mitavat'
    },
	{
        'padas': 'wuBfY ktavatu',
        'sutras': [
			72010,  # ekAcha upadeshe.anudAttAt
        ],
        'result': 'Bftavat'
    },
	{
        'padas': 'duha~ tumun',
        'sutras': [
			73086,	# pugantasyalaghUpadhasya cha
			82032,	# dAderdhAtorghaH
			82040,	# jhaShastathordho.adhaH
			84053,	# jhalAm jash jhashi
        ],
        'result': 'dogdhum'
    },
	{
        'padas': 'yujir ktvA',
        'sutras': [
			72010,  # ekAcha upadeshe anudAttAt
			73086,	# pugantasyalaghUpadhasya cha
			82030,	# choH kuH
			84055,	# khari cha
        ],
        'result': 'yoktvA'
    },
	{
        'padas': 'GfRu~ Ramul',
        'sutras': [
			73086,	# pugantasyalaghUpadhasya cha
        ],
        'result': 'GarRam'
    },
	{
        'padas': 'vi jYA~ ktvA',
        'sutras': [
			22018,	# kugati prAdayaH ('vi' upasargaH ataH samAsaH iti samj~nA)
			71037,	# samAse.anaNYpUrvo lyap
        ],
        'result': 'vijYAya'
    },
	{
        'padas': 'ramu~ GaY',
        'sutras': [
			72116,	# ata upadhAyAH
        ],
        'result': 'rAma'
    },
	{
        'padas': 'ciY ac',
        'sutras': [
			73084,	# sArvadhAtukArdhadhAtukayoH
			61078,	# echo.ayavAyAvaH
        ],
        'result': 'caya'
    },
	{
        'padas': 'zwuY ap',
        'sutras': [
			61064,	# dhAtvAdeH ShaH saH
			73084,	# sArvadhAtukArdhadhAtukayoH
			61078,	# echo.ayavAyAvaH
        ],
        'result': 'stava'
    },
	{
        'padas': 'gamF ktin',
        'sutras': [
			64037,	# anudAttopadeshavanatitanotyAdInAmanunAsika lopo jhali kNiti
        ],
        'result': 'gati'
    },
    {   
		'padas': 'Uha~ a',
        'sutras': [
			41004,	# ajAdyaShTAp (after getting 'Uha' strii pratyaya is added)
			61101,	# akaH savarNe dIrghaH
        ],
        'result': 'UhA'
    },
	{   
		'padas': 'Asa~ yuc',
        'sutras': [
			71001,  # yuvoranAkau
			41004,	# ajAdyaShTAp (after getting 'Asana' strii pratyaya is added)
			61101,	# akaH savarNe dIrghaH
        ],
        'result': 'AsanA'
    },
	{   
		'padas': 'Bikza~ u',
        'sutras': [
        ],
        'result': 'Bikzhu'
    },
	{   
		'padas': 'kfY aR',
        'sutras': [
 			11051,  # uraNraparaH
			22019,	# upapadamatiNG
			24071,	# supodhAtuprAtipadikayoH
        ],
        'result': 'kumBakAra'
    },
	{   
		'padas': 'qudAY aR',
        'sutras': [
 			64064,  # Ato lopa iTi ca
			22019,	# upapadamatiNG
			24071,	# supodhAtuprAtipadikayoH
        ],
        'result': 'goda'
    },
	{   
		'padas': 'graha~ in',
        'sutras': [
			72116,	# ata upadhAyAH
        ],
        'result': 'grAhin'
    },
	{   
		'padas': 'ji kvip',
        'sutras': [
 			61071,	# hrasvasya piti kRiti tuk
			61067,  # verapRiktasya
			22019,	# upapadamatiNG
			24071,	# supodhAtuprAtipadikayoH
        ],
        'result': 'zatrujit'
    },
	{   
		'padas': 'Bidir aN',
        'sutras': [
			61067,  # verapRiktasya
			41004,	# ajAdyaShTAp (after getting 'bhida' strii pratyaya is added)
			61101,	# akaH savarNe dIrghaH
        ],
        'result': 'BidA'
    },
	{   
		'padas': 'pUN SAnan',
        'sutras': [
			31068,	# kartari shap
			73084,	# sArvadhAtukArdhadhAtukayoH
			61078,	# echo.ayavAyAvaH
			72082,	# Ane muk
        ],
        'result': 'pavamAna'
    },
	{   
		'padas': 'Buja~ cAnaS',
        'sutras': [
			31068,	# rudAdibhyaH shnam
			73084,	# shnasorllopaH
			83024,	# nashchApadAntasya jhali
			84058,	# anusvArasya yayi parasavarNaH
        ],
        'result': 'BuYjAna'
    },
	{   
		'padas': 'ejf~ KaS',
        'sutras': [
			73084,	# sArvadhAtukArdhadhAtukayoH
			61078,	# echo.ayavAyAvaH
			63067,	# arurdviShadajantasya mum 
        ],
        'result': 'janamejaya'
    },
	{   
		'padas': 'pA Sa',
        'sutras': [
			73078,	# पाघ्राध्मास्थाम्नादाण्दृश्यर्त्तिसर्त्तिशदसदां पिबजिघ्रधमतिष्ठमनयच्छपश्यर्च्छधौशीयसीदाः |
        ],
        'result': 'piba'
    },
	{   
		'padas': 'cakzi eS',
        'sutras': [
			34015,	# avacakShe ca
			14080,	# teprAgdhAtoH
			31068,	# kartari shap
			61097,  # ato guNe
        ],
        'result': 'avachakzhe'
    },
	{   
		'padas': 'madI~ SaDyai',
        'sutras': [
			72116, 	# ata upadhAyAH
			31068,	# kartari shap
			61097,  # ato guNe
			73084,	# sArvadhAtukArdhadhAtukayoH
			61078,	# echo.ayavAyAvaH		
        ],
        'result': 'mAdayaDyai'
    },
	{   
		'padas': 'pA SaDyain',
        'sutras': [
			73078,	# पाघ्राध्मास्थाम्नादाण्दृश्यर्त्तिसर्त्तिशदसदां पिबजिघ्रधमतिष्ठमनयच्छपश्यर्च्छधौशीयसीदाः
			61097,  # ato guNe
        ],
        'result': 'pibaDyai'
    },
	{   
		'padas': 'Ridi~ vuY',
        'sutras': [
			71058,  # idito num dhAtoH
			71001,  # yuvoranAkau
        ],
        'result': 'nindaka'
    },
	{   
		'padas': 'jvala~ Ra',
        'sutras': [
			72116,  # ata upadhAyAH
        ],
        'result': 'jvAla'
    },
	{   
		'padas': 'gai Ryuw',
        'sutras': [
			71001,  # yuvoranAkau
			61078,	# echo ayavAyAvaH
        ],
        'result': 'gAyana'
	},
	{   
		'padas': 'BU KukaY', # (ADyam+BU+uka, upapadam)
        'sutras': [
			72115, 	# supodhAtuprAtipadikayoH
			72115,	# acho NYNGiti 
			61078,	# echo ayavAyAvaH
			63067, 	# arurdviShadajantasya mum	
        ],
        'result': 'ADyamBAvuka'
	},
	{   
		'padas': 'Baja~ Rvi', # (arDam + Baj + v, upapadam)
        'sutras': [
			61067,	# verapRiktasya
			72115, 	# supodhAtuprAtipadikayoH
			72116, 	# ata upadhAyAH	
        ],
        'result': 'ArDaBAj'
	},
	{   
		'padas': 'vaha~ Yuw', # (kavyam + vah + yu, upapadam)
        'sutras': [
			72115, 	# supodhAtuprAtipadikayoH
			72116, 	# ata upadhAyAH
			71001,	# yuvoranAkau
        ],
        'result': 'kavyavAhana'
	},
	{   
		'padas': 'Samu~ GinuR',
        'sutras': [
			73034, 	# nodAttopadeshasya mAntasyAnAchameH
        ],
        'result': 'Samin'
	},
	{   
		'padas': 'laza~ ukaY',
        'sutras': [
			72116, 	# ata upadhAyAH
        ],
        'result': 'lAzuka'
	},
	{   
		'padas': 'vA uN',
        'sutras': [
			73033, 	# ato yuk chiNkRitoH
        ],
        'result': 'vAyu'
	},
	{   
		'padas': 'zwuY kyap',
        'sutras': [
			61064,	# dhAtvAdeH ShaH saH
			61071, 	# hrasvasya piti kRiti tuk
        ],
        'result': 'stutya'
	},
	{   
		'padas': 'nftI~ zvun',
        'sutras': [
			71001, 	# yuvoranAkau
			73086,	# pugantasyalaghUpadhasya cha
        ],
        'result': 'nartaka'
	},
	{   
		'padas': 'gai Takan',
        'sutras': [
			61045,	# Adecha upadeshe.ashiti
        ],
        'result': 'gAthaka'
	},
	{   
		'padas': 'lUY vun',
        'sutras': [
			71001,	# yuvoranAkau
			73084,	# sArvadhAtukArdhadhAtukayoH
			61078,	# echo.ayavAyAvaH
        ],
        'result': 'lavaka'
	},
	{   
		'padas': 'ji snu',
        'sutras': [
			83059,	# AdeshapratyayayoH
			84001,	# raShAbhyAM no NaH samAnapade
        ],
        'result': 'jizRu'
	},
	{   
		'padas': 'jalpa~ zAkan',
        'sutras': [
        ],
        'result': 'jalpAka'
	},
	{   
		'padas': 'paWa~ Ric',
        'sutras': [
			72116,	# ata upadhAyAH
        ],
        'result': 'pAWi'
	},
	{   
		'padas': 'pAWi kta',
        'sutras': [
        ],
        'result': 'pAWita'
	},
#	{
#		'padas': 'pA san',
#        'sutras': [
#			61009,	 sanyaNGoH
#			74059,	 hrasvaH (pUrvobhyAsaH, achashcha)
#			74079,	 sanyataH
#        ],
#        'result': 'pipAsa'
#	},
	{   
		'padas': 'pipAsa kta',
        'sutras': [
			72035,	# ArdhadhAtukasyeD valAdeH
        ],
        'result': 'pipAsita'
	},
]