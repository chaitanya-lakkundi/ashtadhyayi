import sys
import json

import flask
from flask_cors import CORS
from werkzeug.middleware.proxy_fix import ProxyFix

sys.path.append("..")
from vedavaapi.ashtadhyayi.cmdline import *
from pprint import pprint, pformat
#from .lib.importer import ESImportContext


app = flask.Flask(__name__, instance_relative_config=True)
# fix https mixed content
# disable "proxy_set_header" in nginx if using Proxyfix (proxy_set_header Host $http_host;)
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_port=1)

CORS(app)
app.secret_key = 'Vedavaapi Ashtadhyayi'

try:
    app.config.from_json(filename="config.json")
except FileNotFoundError as e:
    pass

#vv_conf = app.config.get('VV', {})
#client_creds_path = vv_conf.get('client_creds_path', '/opt/content_search/client_creds.json')
#atr_path = vv_conf.get('atr_path', '/opt/content_search/atr.json')
#try:
#    atr = json.loads(open(atr_path, 'rb').read().decode('utf-8'))
#except:
#    atr = {}

set_req_context(ReqContext())
loglevel(0)
script("SLP1")
ashtadhyayi(rebuild=True)

from .api import api_blueprint_v1
app.register_blueprint(api_blueprint_v1, url_prefix='/paias')
