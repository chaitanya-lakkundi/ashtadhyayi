import json

import flask_restx

from .. import ashtadhyayi
from . import api
from examples.interpreter import *

interpret_ns = api.namespace('krdantas', path='/')

admin_ns = api.namespace('admin', path='/admin')
session = {}

@interpret_ns.route('/generator')
class Interpreter(flask_restx.Resource):
    transforms = (
        "ga\mx~ Satf~",
        "paWa~ Satf~",
        "aki~ anIyar",
        'fDu~ kyap',
        'iN tavya',
        'kadi~ tavyat',
        'uN yat',
        'kramu~ tfc',
        'ciY ac',
        'jalpa~ zAkan',
        'vadi~ SAnac',
    )

    get_parser = interpret_ns.parser()
    get_parser.add_argument('padas', type=str, location='args', choices=transforms, required=True)
    get_parser.add_argument('newpada', type=str, location='args', required=False)
    get_parser.add_argument('inscript', type=str, choices=('SLP1', 'ITRANS', 'DEVANAGARI'), location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=('SLP1', 'IAST', 'DEVANAGARI', 'ITRANS'), location='args', default='SLP1', required=False)
    get_parser.add_argument('logging_level', type=str, choices=('0', '1', '2'), location='args', default='0', required=False)

    @interpret_ns.expect(get_parser, validate=True)
    def get(self):
        if 'req' in session:
            set_req_context(session['req'])

        args = self.get_parser.parse_args()
        padas = args['newpada'] if args['newpada'] else args['padas']
        loglevel(args['logging_level'])
        res = interpret(padas=padas.split(), inscr=args['inscript'],
                  outscr=args['outscript'])

        return res, 200

@interpret_ns.route('/analyzer')
class Interpreter(flask_restx.Resource):
    transforms = (
        "ga\\cCat",
        "paWat",
        "aNkanIya",
        'fDya',
        'etavya',
        'kanditavya',
        'avya',
        'kramitf',
#        'caya',
        'jalpAka',
        'vandamAna',
    )

    get_parser = interpret_ns.parser()
    get_parser.add_argument('pada', type=str, location='args', choices=transforms, required=False)
    get_parser.add_argument('newpada', type=str, location='args', required=False)
    get_parser.add_argument('inscript', type=str, choices=('SLP1', 'ITRANS', 'DEVANAGARI'), location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=('SLP1', 'IAST', 'DEVANAGARI', 'ITRANS'), location='args', default='SLP1', required=False)

    @interpret_ns.expect(get_parser, validate=True)
    def get(self):

        args = self.get_parser.parse_args()
        newpada = args['newpada']
        if newpada and args['inscript'] != 'SLP1':
            newpada = xliterate(newpada, from_script=args['inscript'], to_script='SLP1')

        pada = newpada if newpada else args['pada']
        res = analyze(pada=pada, outscr=args['outscript'])

        return res, 200


@admin_ns.route('/rebuild')
class AdminOps(flask_restx.Resource):
    def get(self):
        return { 'status': 'success'}, 200

@admin_ns.route('/login')
class AdminOps(flask_restx.Resource):

    get_parser = admin_ns.parser()
    get_parser.add_argument('inscript', type=str, choices=('SLP1', 'ITRANS', 'DEVANAGARI'), location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=('SLP1', 'IAST', 'DEVANAGARI', 'ITRANS'), location='args', default='SLP1', required=False)
    get_parser.add_argument('logging_level', type=str, choices=('0', '1', '2'), location='args', default='0', required=False)

    @admin_ns.expect(get_parser, validate=True)
    def get(self):
        session['req'] = ReqContext()
        args = self.get_parser.parse_args()
        return { 'status': 'success'}, 200
