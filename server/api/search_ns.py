import json

import elasticsearch
import flask_restx
import flask

from .. import es_context
from . import api


search_ns = api.namespace('search', path='/search')


def remove_nones_in_doc(doc:dict):
    for k, v in list(doc.items()):
        if v is not None:
            return
        doc.pop(k)


@search_ns.route('/doc/<index>/<_id>')
class Doc(flask_restx.Resource):

    get_parser = search_ns.parser()
    get_parser.add_argument('_source_includes', type=str, location='args', required=False)
    get_parser.add_argument('_source_excludes', type=str, location='args', required=False)
    get_parser.add_argument('_source', type=str, location='args', required=False)

    @search_ns.expect(get_parser, validate=True)
    def get(self, index, _id):
        args = self.get_parser.parse_args()
        remove_nones_in_doc(args)
        if index not in (es_context.libs_index, es_context.pages_index, es_context.books_index):
            return {"error": "invalid index name"}, 400
        try:
            doc = es_context.es_client.get(index, _id, params=args)
        except elasticsearch.exceptions.NotFoundError as e:
            return {'error': 'not found'}, 404
        return doc, 200


@search_ns.route('/<index>')
class Search(flask_restx.Resource):

    post_parser = search_ns.parser()
    post_parser.add_argument('from', type=int, location='json', required=False)
    post_parser.add_argument('size', type=int, location='json', required=False)


    @search_ns.expect(post_parser, validate=False)
    def post(self, index):
        # args = self.post_parser.parse_args()
        if index not in (es_context.libs_index, es_context.pages_index, es_context.books_index):
            return {"error": "invalid index name"}, 400
        try:
            resp = es_context.es_client.search(body=flask.request.json, index=index)
        except elasticsearch.exceptions.RequestError as e:
            return { "error": "invalid request", "message": e.info }, 400
        return resp, 200
