from typing import List

from elasticsearch import Elasticsearch


vv_html_analyzer = {
    "type": "custom",
    "char_filter": ["html_strip"],
    "tokenizer": "standard",
    "filter": ["lowercase", "stop"]
}

# Any custom analyzers/filters/char_filters will have their name prifixed with vv
vv_analysis = {
    "analyzer": {
        "vvHtmlAnalyzer": vv_html_analyzer,
    }
}


libs_settings = { "analysis": vv_analysis }
libs_mappings = {
    "properties": {
        "library_id": { "type": "keyword" },
        "name": { "type": "text" }
    }
}
libs_body = {"settings": libs_settings, "mappings": libs_mappings}


books_settings = { "analysis": vv_analysis }
books_mappings = {
    "properties": {
        "library_id": { "type": "keyword" },
        "book_id": { "type": "keyword" },
        "name": { "type": "text" },
        "author": { "type": "text" }
    }
}
books_body = {"settings": books_settings, "mappings": books_mappings}


pages_settings = { "analysis": vv_analysis }
pages_mappings = {
    "properties": {
        "library_id": { "type": "keyword" },
        "book_id": { "type": "keyword" },
        "page_id": { "type": "keyword" },
        "page_no": { "type": "long" },
        "content": { "type": "text", "analyzer": "vvHtmlAnalyzer", "index_options": "offsets" }  # We may leave index_options default. But offsets helps in efficient highlighting
    }
}
pages_body = { "settings": pages_settings, "mappings": pages_mappings }


class ESImportContext(object):

    def __init__(
            self, es_hosts: List[str], libs_index='libs', books_index='books', pages_index='pages'):
        self.es_hosts = es_hosts
        self.es_client = Elasticsearch(hosts=es_hosts)
        self.libs_index = libs_index
        self.books_index = books_index
        self.pages_index = pages_index

        body_map = { libs_index: libs_body, books_index: books_body, pages_index: pages_body }
        for index in (libs_index, books_index, pages_index):
            self.es_client.indices.create(index=index, ignore=400, body=body_map[index])

    def to_json(self):
        return {
            "es_hosts": self.es_hosts,
            "libs_index": self.libs_index,
            "books_index": self.books_index,
            "pages_index": self.pages_index,
        }

    @classmethod
    def from_json(cls, j):
        return cls(j['es_hosts'], j['libs_index'], j['books_index'], j['pages_index'])
