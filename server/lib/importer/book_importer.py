import json

from requests import HTTPError
from textract.tools import batch_it
from vedavaapi.client import VedavaapiSession

from textract.tasks.celery_helper import default_update_state, VedavaapiRuntimeException
from textract import books
from textract.books.exporters.html import content_from_regions

from . import ESImportContext


# noinspection PyUnusedLocal
def import_pages(
        page_ids,
        vc: VedavaapiSession,
        es_import_context: ESImportContext,
        book_id,
        library_id=None,
        batch_context=None, update_state=None, **kwargs):
    update_state = update_state or default_update_state
    batch_context = batch_context or {}
    # print(batch_context)
    pages_index_start = (batch_context.get('current_batch', 1) - 1) * batch_context.get('batch_size', 1)

    segments_graph, layout_graph = books.get_regions_layout_graphs(vc, page_ids)

    page_docs = []

    def get_page_doc(_id, page_no, content):
        return {
            "page_id": _id,
            "page_no": page_no,
            "book_id": book_id,
            "library_id": library_id,
            "content": content
        }

    for i, page_id in enumerate(page_ids):
        ordered_regions = books.get_ordered_regions(page_id, layout_graph, segments_graph)
        if ordered_regions is None:
            continue
        page_content = content_from_regions(
            segments_graph, ordered_regions, html=True, add_class=False, newline2br=False, add_quotes=False)
        page_docs.append(get_page_doc(page_id, pages_index_start + i, page_content))

    es_bulk_index_body = ''
    for doc in page_docs:
        es_bulk_index_body += json.dumps({
            "index": {"_index": es_import_context.pages_index, "_id": doc['page_id']}
        })
        es_bulk_index_body += '\n'
        es_bulk_index_body += json.dumps(doc)
        es_bulk_index_body += '\n'

    if not es_bulk_index_body:
        return

    try:
        #  print('es_bulk_start')
        r = es_import_context.es_client.bulk(es_bulk_index_body)
        #  pprint(r)
        #  print('es_bulk_success', r)
    except Exception as e:
        #  print('es_bulk_error', e)
        raise VedavaapiRuntimeException(
            update_state=update_state, exc_message="error in indexing pages", exc_type='Elastic Error', exception=e)


def import_book(
        vc: VedavaapiSession,
        es_import_context: ESImportContext,
        book_id: str, update_state=None):
    update_state = update_state or default_update_state

    try:
        book_resp = vc.get('objstore/v1/resources/{}'.format(book_id))
        book_resp.raise_for_status()
        book = book_resp.json()
        book_doc = {
            "book_id": book_id, "name": book.get('title', ''),
            "author": book.get('author', ''), "library_id": book['source']
        }
        es_import_context.es_client.index(
            es_import_context.books_index, json.dumps(book_doc), id=book_id)
    except HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state, exc_message='error in getting book details:' + str(e.response.json()), exc_type='HTTPError', exception=e)

    resolved_page_ids = books.resolve_page_ids(
        vc, book_id=book_id, update_state=update_state)

    update_state(
        state='PROGRESS', meta={"status": "initializing getting pages content"}
    )

    batch_size = 15
    index_pages_batch_wise = batch_it(
        batch_size, update_state=update_state,
        master_state='PROGRESS', master_status='retrieving pages and their annotations')(import_pages)
    try:
        index_pages_batch_wise(resolved_page_ids, vc, es_import_context, book_id, book['source'])
    except VedavaapiRuntimeException as e:
        raise VedavaapiRuntimeException(
            update_state=update_state, exc_message=e.message, exc_type=e.exc_type, exception=e.exception)
