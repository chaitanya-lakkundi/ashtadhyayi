#!/usr/bin/python
# -*- coding: utf-8 -*-

import functools
from .utils import *

from collections.abc import Sequence

class Pada:
    def __init__(self, pada='', split_by=[], attrs={}):
        self.pada = pada
        self.attrs = copy.deepcopy(attrs)

    def from_dict(self, obj):
        for k,v in obj.items():
            self.k = v

    def __getitem__(self, name):
        return self.__getattr__(name)

    @classmethod
    def split(cls, pada, split_by=''):
        pattern = r'[^{schars}][{schars}]*'.format(schars=split_by)
        res = re.findall(pattern, pada)
        #res2 = [res[0]] + list(filter(lambda s: s != 'a', res[1:]))
        return res

    @classmethod
    def nosvaras(cls, pada):
        return re.sub('[\\\/^]', '', pada)

    @classmethod
    def find_varna(cls, srchv, vlist, exact=False):
        if exact:
            return [srchv in vlist]
        srchv = re.sub('[~\\\^]*$', '', srchv)
        for v in vlist:
            v = re.sub('[~\\\^]*$', '', v)
            if srchv == v:
                return True
        return False

# A sequence of morphemes to be analyzed
class PadaSequence(Sequence):
    def __init__(self, myseq=None, *, srcseq=None,  padas=[], pinds = None, vinds = None):
        if myseq:
            self.parts = myseq['parts']
            self.labels = myseq['labels']
            self.views = myseq['views']
            self.prev_transforms = myseq['prev_transforms']
        else:
            self.parts = {'padas': [], 'varnas': []}
            self.labels = {}
            self.views = {'padas' : [], 'varnas': []}
            self.prev_transforms = []

        if srcseq:
            self.parts = copy.deepcopy(srcseq.parts)
            self.labels = copy.deepcopy(srcseq.labels)
            self.views = copy.deepcopy(srcseq.views)
            self.prev_transforms = srcseq.prev_transforms

        if padas:
            all_varnas = []
            for p in padas:
                pvarnas = Pada.split(p.strip(), '/3\\\^~')
                self.parts['padas'].append({'pada': p,
                                            'pos': (len(all_varnas),
                                                    len(pvarnas) + len(all_varnas))})
                all_varnas.extend(pvarnas)

            self.parts['varnas'] = all_varnas

        # If both pinds and vinds weren't supplied set them to full set
        if pinds == None and vinds == None:
            pinds = list(range(0, len(self.parts['padas'])))
#            vinds = list(range(0, len(self.parts['varnas'])))

        if pinds != None:
            self.views['padas'] = pinds
        elif 'padas' in self.views:
            del self.views['padas']

        if vinds != None:
            self.views['varnas'] = vinds
        elif 'varnas' in self.views:
            del self.views['varnas']

    def subset(self, *, pinds=None, vinds=None):
        newpseq = PadaSequence(srcseq=self, pinds=pinds, vinds=vinds)
#        if not pinds and 'padas' in newpseq.views:
#            newpseq.views['padas'] = []
#        if not vinds and 'varnas' in newpseq.views:
#            newpseq.views['varnas'] = []
#        debuglog("subset: ", newpseq.views)
        debuglog(f"subset of {self} is {newpseq}")
        return newpseq

    def delvarnas(self, vinds):
        # Compute the new varna inds as a result of deletion
        new_vinds = {}
        ndeleted = 0
        for i in range(len(self.parts['varnas'])):
            new_vinds[i] = i - ndeleted if i in vinds else None
            if i in vinds:
                new_vinds[i] = None
                ndeleted += 1
            else:
                new_vinds[i] = i - ndeleted

        trunc_pos = lambda l: [new_vinds[i] for i in l if new_vinds[i]!=None]
        debuglog(f"delvarnas: new_inds = {new_vinds}", level=1)

        # Adjust varnas list
        for i in sorted(vinds, reverse=True):
            del self.parts['varnas'][i]
        # Adjust varna ranges in padas
        maxi = 0
        for i,p in enumerate(self.parts['padas']):
            (o1, o2) = p['pos']
            if o1 == o2:
                (n1, n2) = (maxi, maxi)
            else:
                (n1, n2) = (new_vinds[o1], new_vinds[o2-1])
                debuglog(f"delvarnas {o1},{o2} -> {n1},{n2}", level=1)
                if n2 != None:
                    n2 += 1
                if n1 == None:
                    n1 = maxi
                if n2 == None:
                    for j in reversed(range(o1,o2)):
                        debuglog(f"{j}->{new_vinds[j]}")
                        if new_vinds[j] != None:
                            n2 = new_vinds[j]+1
                            break
                    if n2 == None:
                        n2 = maxi
            maxi = n2
            debuglog(f"delvarnas {o1},{o2} -> {n1},{n2}", level=1)
            self.parts['padas'][i]['pos'] = (n1, n2)

        # Adjust varna indices in labels
        dellabels = []
        for l,linfo in self.labels.items():
            if 'varnas' in linfo:
                delinds = set()
                for i,e in enumerate(linfo['varnas']):
                    newpos = trunc_pos(e['pos'])
                    if newpos:
                        linfo['varnas'][i]['pos'] = newpos
                    else:
                        delinds.add(i)

                for i in sorted(list(delinds), reverse=True):
                    del linfo['varnas'][i]
                if not linfo['varnas']:
                    dellabels.append(l)
        for l in dellabels:
            del self.labels[l]

        # Adjust varna indices in views
        if 'varnas' in self.views:
            self.views['varnas'] = trunc_pos(self.views['varnas'])
        debuglog("delvarna returned ", self)

    # replace = 0 means insert, 1 means replace purva, 2 means purva & para
    def upsertvarnas(self, vinds, insvarnas, replace=0):
        debuglog(f"upsertvarnas: In {self.parts['varnas']} replace = {replace} old_vinds {vinds}, new varnas {insvarnas}")
        # First delete old varnas
        if replace and not insvarnas:
#            if len(vinds) == len(insvarnas):
#                self.parts['varnas'][vinds[0]:vinds[0]+len(vinds)] = insvarnas
#                return
            self.delvarnas(vinds)
            return

        pos = vinds[0]

        # Compute the new indices of the varnas after insertion
        pada_vinds = [list(range(*p['pos'])) for p in self.parts['padas']]
        debuglog(f"upsertvarnas: old pada_vinds={pada_vinds}")
        oldlen = len(vinds) if replace else 0
        newlen = len(insvarnas)
        new_vinds = {}
        newvarnas = []
        curpind = 0
        pada_newvinds = [[] for i in self.parts['padas']]
        for i,v in enumerate(self.parts['varnas']):
            while not pada_vinds[curpind]:
                curpind += 1
            if i not in pada_vinds[curpind]:
                curpind += 1
            if i < vinds[0]:
                new_vinds[i] = (i, curpind)
                newvarnas.append(v)
                pada_newvinds[curpind].append(i)
                continue
            if i in vinds:
                if replace:
                    new_vinds[i] = (-1, -1)
                else:
                    new_vinds[i] = (i, curpind)
                    newvarnas.append(v)
                    pada_newvinds[curpind].append(i)
                if i == vinds[0] and insvarnas:
                    newvarnas.extend(insvarnas)
                if i == vinds[-1]:
                    pada_newvinds[curpind].extend(list(range(len(newvarnas)-newlen, len(newvarnas))))
            if i > vinds[-1]:
                new_vinds[i] = (i + newlen - oldlen, curpind)
                pada_newvinds[curpind].append(len(newvarnas))
                newvarnas.append(v)

        debuglog(f"upsertvarnas: new pada_vinds={pada_newvinds}")
        self.parts['varnas'] = newvarnas

        # Adjust varna ranges in padas
        p_vind = 0
        for i,vlist in enumerate(pada_newvinds):
            if not vlist:
                self.parts['padas'][i]['pos'] = (p_vind, p_vind)
            else:
                p_vind = vlist[-1]+1
                self.parts['padas'][i]['pos'] = (vlist[0], p_vind)

        new_pos = lambda l: [new_vinds[i][0] for i in l if new_vinds[i][0] >= 0]

        debuglog(f"upsertvarnas: new varna inds: {new_vinds} ")

        debuglog(f"upsertvarnas: parts = {self.parts} ")

        # Adjust varna indices in labels
        for l,linfo in self.labels.items():
            if 'varnas' in linfo:
                for i,e in enumerate(linfo['varnas']):
                    linfo['varnas'][i]['pos'] = new_pos(e['pos'])

        # Adjust varna indices in views
        if 'varnas' in self.views:
            self.views['varnas'] = new_pos(self.views['varnas'])
            self.views['varnas'][pos:pos] = [i+pos for i in range(len(insvarnas))]

        # Finally, install the new varnas
        debuglog(f"pseq.upsertvarnas: parts={ppformat(self.parts)}, "
                 f"views={ppformat(self.views)}, labels={ppformat(self.labels)}")

    # Incorporate pada sequence pseq into self by
    # either replacing or inserting at given pos
    def modpada(self, pseq, pos, replace=None):
        pvarnas = pseq.varnas()
        vind = self.parts['padas'][pos]['pos'][1]
        pind = pos+1
        # adjust varna pointers everywhere to reflect the insertion
        for i in range(pind, len(self.parts['padas'])):
            pvrange = self.parts['padas'][i]['pos']
            self.parts['padas'][i]['pos'] = \
                (pvrange[0] + len(pvarnas), pvrange[1] + len(pvarnas))
        self.parts['padas'].insert(pind, ({'pada': pseq.padas(0)['pada'],
                                           'pos': (vind,
                                                   vind + len(pvarnas))}))
        self.parts['varnas'][vind:vind] = pvarnas
        debuglog("pseq.modify: parts {}".format(ppformat(self.parts)))

        self.views['padas'] = [i+1 if i >= pind else i for i in self.views['padas']]
        self.views['padas'].insert(pind, pind)
        if 'varnas' in self.views:
            self.views['varnas'] = [i+len(pvarnas) if i >= vind else i for i in self.views['varnas']]
            self.views['varnas'] = sorted(self.views['varnas'] + list(range(vind, vind + len(pvarnas))))
            debuglog("pseq.modify: views {}".format(ppformat(self.views)))

        for l,e in self.labels.items():
            #print(f"pseq.modify before: {l}: {e}")
            if 'padas' in e:
                for linfo in e['padas']:
                    linfo['pos'] = [i+1 if i >= pind else i for i in linfo['pos']]
            #print(f"pseq.modify after: {l}: {e}")
            if 'varnas' in e:
                for linfo in e['varnas']:
                    linfo['pos'] = [i+len(pvarnas) if i >= vind else i for i in linfo['pos']]

        # Transfer labels from pseq to self
        for l,e in pseq.labels.items():
            if 'padas' in e:
                for linfo in e['padas']:
                    pos = None
                    if 'pos' in linfo:
                        pos = [i+pind for i in linfo['pos']]
                    self.add_label(f"padas.{l}", positions=pos,
                                   sutra_id=linfo['sutra_id'] if 'sutra_id' in linfo else None,
                                value=linfo['val'] if 'val' in linfo else None)
            if 'varnas' in e:
                for linfo in e['varnas']:
                    pos = None
                    if 'pos' in linfo:
                        pos = [i + vind for i in linfo['pos']]
                    self.add_label(f"varnas.{l}", positions=pos,
                                   sutra_id=linfo['sutra_id'] if 'sutra_id' in linfo else None,
                                   value=linfo['val'] if 'val' in linfo else None)

        for hist in pseq.prev_transforms:
            self.prev_transforms.append(hist)
        return pind

    def varnas(self, ind=-1):
        return self.getParts('varnas') if ind < 0 else self.getPart('varnas', ind)

    def padas(self, ind=-1):
        return self.getParts('padas') if ind < 0 else self.getPart('padas', ind)

    # Derive pada view from varna view and vice versa
    def _derive_view(self, part):
        if part in self.views:
            return self.views[part]

        if part == 'padas':
            if 'varnas' not in self.views:
                return []
            vinds = self.views['varnas']
            pinds = [i for i, p in enumerate(self.parts['padas']) if set(vinds) & set(range(*p['pos']))]
            return pinds
        if part == 'varnas':
            if 'padas' not in self.views:
                return []
            pinds = self.views['padas']
            vinds = functools.reduce(lambda x, y: x + y,
                                     [list(range(*self.parts['padas'][i]['pos'])) for i in pinds])
            return vinds
        return []

    def getPart(self, part, ind):
        if ind >= 0:
            return self.parts[part][ind]
        view = self._derive_view(part)
        if not view:
            debuglog("getPart error:", ppformat(self.views))

    def getParts(self, part):
        view = self._derive_view(part)
        return [self.parts[part][i] for i in view]

    def __getitem__(self, ind=0):
        return self.varnas(ind)
    def __len__(self):
        return len(self.parts['varnas'])

    def push(self, sutra_id):
        pseq_old = copy.deepcopy(vars(self))
        pseq_old['prev_transforms'] = []
        prev_transform = {'sutra_id': sutra_id, 'pseq': pseq_old }
        self.prev_transforms.append(prev_transform)

    def pop(self):
        if not self.prev_transforms:
            return None
        clone_xforms = copy.deepcopy(self.prev_transforms)
        newp = PadaSequence(clone_xforms[-1]['pseq'])
        newp.prev_transforms = clone_xforms
        newp.prev_transforms.pop()
        return newp

    def add_label(self, label, positions=None, *, sutra_id=None, value=None):
        label_rec = {}
        if positions:
            label_rec['pos'] = positions
        if sutra_id:
            label_rec['sutra_id'] = sutra_id
        if value:
            label_rec['val'] = value

        debuglog("Add label {} to {}: {}".format(label, str(self),
                                                 ppformat(label_rec)))
        c = label.split('.')
        if c[1] not in self.labels:
            self.labels[c[1]] = {}
        if c[0] not in self.labels[c[1]]:
            self.labels[c[1]][c[0]] = []
        self.labels[c[1]][c[0]].append(label_rec)
        #debuglog("added {}: {}".format(label, label_rec), level=0)
        return self

    def lookup_label(self, label):
#        linfo = None
#        pseq = self
        if label in self.labels:
            return [self]
        else:
            return [PadaSequence(t['pseq'])
                      for t in reversed(self.prev_transforms) if label in t['pseq']['labels']]
#            for t in reversed(self.prev_transforms):
#                debuglog(f"lookup_label({label}: prev_seq={ppformat(t['pseq'])}", level=2)
#                if label in t['pseq']['labels']:
#                    linfo = t['pseq']['labels'][label]
#                    pseq = PadaSequence(t['pseq'])
#                    debuglog(f"lookup_label({label}: linfo={linfo}", level=2)
        #return pseq, linfo

    # Return a copy of self with only those padas labelled with given label
    def find_label(self, label):
        debuglog("find_label: ", label, self)
        retlist = []
        views = {}
        matchseqs = self.lookup_label(label)
        if not matchseqs:
            return None
        pseq = matchseqs[0]
        linfo = pseq.labels[label]
#        pseq, linfo = self.lookup_label(label)
#        if not linfo:
#            return None

        log("find_label: ", label, pseq, linfo)
        pinds = None
        vinds = None
        if 'val' in linfo:
            if linfo['type'] == 'varnas':
                vinds = linfo['val']['val']
            elif linfo['type'] == 'padas':
                pinds = linfo['val']['val']
        if 'padas' in linfo:
            for p in linfo['padas']:
                if 'val' in p and isinstance(p['val'], dict):
                    log(f"find_label: label={label} {p}")
                    if p['val']['type'] == 'varnas':
                        vinds = p['val']['val']
                    elif p['val']['type'] == 'padas':
                        pinds = p['val']['val']
            if not (vinds or pinds):
#                pinds = sorted(pinds & set([p['pos'][0] for p in linfo['padas']]))
                pinds = reduce(lambda x,y: x | y,
                                         [set(p['pos']) for p in linfo['padas']])
                log(f"find_label: pinds={pinds} in {pseq}")
                if self == pseq:
                    pinds &= set(self._derive_view('padas'))
                pinds = sorted(pinds)
                log(f"find_label: pinds={pinds}")
        if 'varnas' in linfo:
            vinds = set(self._derive_view('varnas'))
            recvarnas = functools.reduce(lambda x, y: set(x) | set(y),
                                [rec['pos'] for rec in linfo['varnas']])
            debuglog(f"find_label view={vinds} label={recvarnas}")
            vinds = sorted(vinds & set(recvarnas))
        if pinds == [] and not vinds:
            return None
        if vinds == [] and not pinds:
            return None
        log("found {}: {} {}".format(
                label,
                "padas=" + str(ppformat([pseq.padas(i)['pada'] for i in pinds])) if pinds else "",
                "varnas=" + str(ppformat([pseq.varnas(i) for i in vinds])) if vinds else ""))
        return pseq.subset(pinds=pinds, vinds=vinds)

    def intersect(self, pseq):
        if not pseq:
            return None
        common_pinds = None
        common_vinds = None
        for i in self._derive_view('padas'):
            p1 = self.padas(i)
            for p2 in pseq.padas():
                if p1['pada'] != p2['pada']:
                    continue
                if 'padas' in self.views:
                    if not common_pinds:
                        common_pinds = []
                    common_pinds.append(i)
                if 'varnas' in self.views:
                    vpada1 = "".join([self.varnas(i) for i in range(*p1['pos'])])
                    vpada2 = "".join([pseq.varnas(i) for i in range(*p2['pos'])])
                    if vpada1 != vpada2:
                        continue
                    vinds1 = sorted(set(range(*p1['pos'])) & set(self.views['varnas']))
                    vinds1 = [i-p1['pos'][0] for i in vinds1]
                    if not common_vinds:
                        common_vinds = []
                    vinds2 = sorted(set(range(*p2['pos'])) & set(pseq._derive_view('varnas')))
                    vinds2 = [i-p2['pos'][0] for i in vinds2]
                    vinds = sorted(set(vinds1) & set(vinds2))
                    if vinds:
                        common_vinds.extend([i+p2['pos'][0] for i in vinds])
                break
        if not (common_pinds or common_vinds):
            return None
        log(f"_intersect: {self} {pseq} pinds={common_pinds}, vinds={common_vinds}")
        return self.subset(pinds=common_pinds, vinds=common_vinds)

        for part in ['padas', 'varnas']:
            common_inds[part] = None
            if part not in self.views:
                continue
            debuglog(f"intersect {part} {ppformat(self.views)}, {ppformat(pseq.views)}")
            common_inds[part] = sorted(set(self.views[part]) & set(pseq._derive_view(part)))
            if not common_inds[part]:
                return None
        #debuglog(f"intersect {self.views} {pseq.views} {common_inds}")
        return self.subset(pinds=common_inds['padas'], vinds=common_inds['varnas'])

    def intersect_old(self, pseq):
        common_inds = {}
        for part in ['padas', 'varnas']:
            common_inds[part] = None
            if (part not in self.views) or (part not in pseq.views):
                continue
            debuglog(f"intersect {part} {ppformat(self.views)}, {ppformat(pseq.views)}", level=0)
            common_inds[part] = sorted(set(self.views[part]) & set(pseq.views[part]))
            if not common_inds[part]:
                return None
        debuglog(f"intersect {self.views} {pseq.views} {common_inds}")
        return self.subset(pinds=common_inds['padas'], vinds=common_inds['varnas'])

    def __repr__(self):
        x = vars(self)
        xforms = x['prev_transforms']
        x['prev_transforms'] = []
        ret = "Pseq-> {}".format(ppformat(x))
        x['prev_transforms'] = xforms
        return ret

    def toJSON(self, hist=True):
        x = vars(self)
        ret = "Pseq-> {}".format(ppformat(x))
        return ret

    def __str__(self):
        vpadas = []
        varnas = self.parts['varnas']
        for p in self.padas():
            vpada = []
            for j in range(0, len(varnas)):
                if j in range(*p['pos']):
                    vpada.append(self.varnas(j))
            vpadas.append("".join(vpada))

        return "Pseq: {} - {} - {}".format(
            ppformat([p['pada'] for p in self.padas()]),
            ppformat(vpadas),
            ppformat(" ".join(self.varnas())))