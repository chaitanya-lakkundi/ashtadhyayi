#!/usr/bin/python
# -*- coding: utf-8 -*-

import functools
import copy
import inspect
from .subanta import *
from .shiksha import *
from .padaseq import *

class Rule:
    def __init__(self, ashtadhyayi, rule = None):
        self.a = ashtadhyayi
        self.rule = rule

        self.IF = self._if
        self.IFNOT = self._ifnot
        self.PIPE = self._pipe
        self.SAMJNA_COMPS = self._samjna_comps
        self.GEN_SAMJNA = self._gen_samjna
        self.GEN_SEQ = self._gen_sequence
        self.LOCATE_FIRST = self._locate_first_upadesha
        self.LOCATE_LAST = self._locate_last_upadesha
        self.ANGA = self._find_anga

        #global IF

        # Key should be in SLP1 format
        self.predefined_funcs = {
            'tatkAla': lambda x, m: self.retvarnas(x.views['varnas']),
            'tat' : lambda x, m: None,
            'antya' : lambda x, m: self._antya(x),
            'anta' : lambda x, m: self._anta(x),
            'Adi' : lambda x, m: self._adi(x),
            r'(t)apara': lambda x, m: self.x_para(x, m),
            r'(.+?)d?it$': lambda x, m: self.x_it(x, m),
            'SabdasaMjYA': lambda x, m: self.retvarnas([0] if self.a.is_samjna(x.padas(0)['pada']) else None),
            'Sabda': lambda x, m: PadaSequence(padas=[x.padas(0)['pada']]).add_label('padas.literal', positions=[0], value=True),
            'anunAsika': lambda x, m: self.retvarnas(self.a.is_anunAsika(x)),
            'viDi': lambda x, m: None,
#            'aNga': lambda x, m: x,
            'savarRa': lambda x, m: self.retvarnas(x.views['varnas']),
            'upadeSa': lambda x, m: self.check_upadesa(x),
            'para' : lambda x, m: self._paraOf(x),
            'pUrva' : lambda x, m: self._purvaOf(x),
            r'(DAtu)': lambda x, m: x.intersect(self.a.is_category(x, m.group(1))),
            'saMhitA': lambda x, m: x,
            'pada': lambda x, m: None, #x.subset(pinds=x._derive_view('padas')),
            r'^(.)[\\\^~]?$': lambda x, m: \
                self.retvarnas([i for i in x._derive_view('varnas') if x.varnas(i) in Siksha.variants(m.group(1))])
        }

    def check_upadesa(self, pseq):
        res = self.a.is_upadesha(pseq)
        if not res:
            return None

        oldseqs = [PadaSequence(t['pseq'])
                for t in reversed(pseq.prev_transforms) if 'upadeSa' in t['pseq']['labels']]
        if oldseqs:
            return None
        return res

        pinds = []
        for i,p in enumerate(pseq.padas()):
            vpada = [pseq.varnas(j) for j in range(0, len(pseq.parts['varnas'])) \
                        if j in range(*p['pos'])]
            if p['pada'] == "".join(vpada):
                pinds.append(i)
        return pseq.subset(pinds=pinds) if pinds else None

    def retvarnas(self, pos):
        return { 'type': 'varnas', 'pos': pos, 'val': None } if pos else None

    # taparaH ityaadi
    def x_para(self, pseq, m):
        match = []
        for i in self._antya(pseq)['pos']:
#            print(i)
#            print(ppformat(pseq))
#            print(pseq.parts['varnas'][i])
#            print(m.group(1))
            if m.group(1) == pseq.parts['varnas'][i]:
                if i-1 in [pseq.padas(j)['pos'][0] for j in pseq._derive_view('padas')]:
                    match.append(i-1)
#        self.debuglog("{}-para = {}".format(m.group(1), match))
        return self.retvarnas(match)

    # Return padas in pseq whose 'it' varnas match those in m.group(1)
    def x_it(self, pseq, m):
        match = []
        matchseqs = pseq.lookup_label('it')
        if not matchseqs:
            return None

        newpinds = set()
        for mseq in matchseqs:
            vinds = []
            for linfo in mseq.labels['it']['varnas']:
                vinds.extend([i for i in linfo['pos']
                         if re.sub(r'[\\\^~]', '', mseq.varnas(i)) in m.group(1)])
                debuglog("{}-it = {}".format(m.group(1), vinds))

            pinds = [i for i, p in enumerate(mseq.parts['padas'])
                     if set(vinds) & set(range(*p['pos']))]
            if mseq == pseq:
                return pinds

            for i in pinds:
                for j in pseq._derive_view('padas'):
                    if mseq.padas(i)['pada'] == pseq.padas(j)['pada']:
                        newpinds.add(j)
        if not newpinds:
            return None

        retseq = pseq.subset(pinds=sorted(newpinds))
        if 'varnas' in pseq.views:
            retseq = pseq.intersect(retseq)
            return self.retvarnas(retseq.views['varnas'])
        else:
            pos = retseq.views['padas']
            return {'type': 'padas', 'pos': pos, 'val': None} if pos else None

        match = []
        for linfo in matchseq.labels['it']['varnas']:
            self.debuglog("{}".format(ppformat(linfo)))
            for i in linfo['pos']:
                if re.sub(r'[\\\^~]', '', matchseq.varnas(i)) in m.group(1):
                    match.append(i)
                self.debuglog("{}-it = {}".format(m.group(1), match))
        if match:
#            print(f"{m.groups(1)}_it: matchseq = {ppformat(matchseq)}")
#            print(f"{m.groups(1)}_it: pseq = {ppformat(pseq)}")
            pinds = [i for i, p in enumerate(matchseq.parts['padas']) if set(match) & set(range(*p['pos']))]
            if len(matchseq.parts['padas']) != len(pseq.parts['padas']):
                # If it match was found in a prior transform
                # return the matching padas in latest sequence
                newpinds = []
                for i in pinds:
                    for j in pseq._derive_view('padas'):
                        if matchseq.padas(i)['pada'] == pseq.padas(j)['pada']:
                            newpinds.append(j)
                pinds = newpinds
            retseq = pseq.subset(pinds=pinds)
            if 'varnas' in pseq.views:
                retseq = pseq.intersect(retseq)
                return self.retvarnas(retseq.views['varnas'])
            else:
                pos = retseq.views['padas']
                return {'type': 'padas', 'pos': pos, 'val': None} if pos else None
        return None

    def _antya(self, pseq):
        #self.debuglog(ppformat(pseq), level=0)
        pos = [p['pos'][1]-1 for p in pseq.padas()]
        return self.retvarnas(pos)

    def _anta(self, pseq):
        #self.debuglog(ppformat(pseq), level=0)
        if 'varnas' in pseq.views:
            vlist = pseq.views['varnas']
            result = []
            for p in pseq.padas():
                if p['pos'][0] == p['pos'][1]:
                    continue
                #antya_vind = sorted(set(range(*p['pos'])))[-1]
                antya_vind = sorted(set(vlist) & set(range(*p['pos'])))[-1]
                if antya_vind in vlist:
                    result.append(antya_vind)
            self.debuglog("_anta(varnas): {} - {}".format(pseq, result))
            return self.retvarnas(result) if result else None
        elif 'padas' in pseq.views:
            pos = [i-1 for i in pseq.views['padas'] if i > 0]
            res = {'type': 'padas', 'pos': pos, 'val': None} if pos else None
            self.debuglog("_anta(padas): {} - {}".format(pseq, res))
            return res

    def _paraOf(self, pseq):
        if 'padas' in pseq.views:
            pos = [i+1 for i in pseq.views['padas'] if i < len(pseq.parts['padas'])]
            return pseq.subset(pinds=pos) if pos else None
        return None

    def _purvaOf(self, pseq):
        if 'varnas' in pseq.views:
            pos = [i-1 for i in pseq.views['varnas'] if i >= 1]
            return pseq.subset(vinds=pos) if pos else None
        return None

    def _adi(self, pseq):
        pos = [p['pos'][0] for p in pseq.padas()]
        return self.retvarnas(pos)

    def debuglog(self, *msg, level=1):
        debuglog(*msg, level=level)

    @logged_func
    def _apply_func(self, pseq, func_desc):
        self.debuglog("apply_func ", ppformat(func_desc), level=2)
        if isinstance(func_desc, list):
            f = self._apply_func(pseq, func_desc[0])
            return f(pseq, func_desc[1:]) if f else None
        elif isinstance(func_desc, dict):
            return self._interpret_pada(pseq, func_desc)
        elif isinstance(func_desc, str):
            self.debuglog("apply_func: ", func_desc, level=1)
            f = eval("self."+func_desc)
            return f
        else:
            return None

    def find_literal_match(self, pseq, f_pada):
        vind = pseq._derive_view('varnas')[0]

        pada = "".join(pseq.parts['varnas'][vind:])
        pada = Pada.nosvaras(pada)
        self.debuglog("literal: {} in {}".format(f_pada, pada))
        f_vlist = Pada.split(f_pada.strip(), '\\\^~')
        i = pada.find(f_pada)
        if i < 0:
            return None
        plist = Pada.split(pada[:i], "\\\^~")
        vinds = [i for i in range(len(plist), len(f_vlist))]
        self.debuglog("plist = {}, vlist = {}, vinds={}".format(plist, f_vlist, vinds))
        pseq.add_label("{}.{}".format('varnas', f_pada), positions=vinds)
        return pseq.find_label(f_pada)

    # f_pada input must be in praatipadika form
    def _interpret_singlepada(self, pseq: PadaSequence, f_desc, c=None):
        f_pada = c if c else Subanta.praatipadikam(f_desc)
        if 'skip' in f_desc:
            debuglog(f"_interpret_singlepada: Ignoring {f_pada}")
            return pseq
        if 'tags' in f_desc and f_pada in f_desc['tags']:
            ptag = f_desc['tags'][f_pada]
            f_pada = ptag['val']
            if 'reject' in ptag:
                return None
            if ptag['literal']:
                return self.find_literal_match(pseq, f_pada)
            else:
                debuglog(f"_interpret_pada (tag): f_desc = {f_desc}")
                new_fdesc = copy.deepcopy(f_desc)
                del new_fdesc['tags']
                new_fdesc['pada'] = f_pada
                debuglog(f"_interpret_pada (tag): new_fdesc = {new_fdesc}")
                if c:
                    new_fdesc['vibhakti'] = 0
                return self._interpret_pada(pseq, new_fdesc)

        self.debuglog("Interpreting single pada ", ppformat(f_pada))

        if f_pada == 'tat':
            return pseq
        if f_pada == 'viDi':
            return None
        for f in self.predefined_funcs:
            m = re.match(f, f_pada)
            if m:
                self.debuglog("matched {} against predef {}".format(pseq, f))
                res = self.predefined_funcs[f](pseq, m)
                self.debuglog("{} returned {}".format(f, res), level=2)
                if not res:
                    return None
                if isinstance(res, dict):
                    if f_pada not in pseq.labels:
                        pseq.add_label("{}.{}".format(res['type'], f_pada),
                                       positions=res['pos'], value=res['val'])
                elif isinstance(res, PadaSequence):
                    self.debuglog(f"_interpret_singlepada predef {ppformat(res.views)}")
                    return res
                break

        # f_pada already evaluated on pada_desc before?
        labelval = pseq.find_label(f_pada)
        if labelval:
            if f_pada not in pseq.labels:
                debuglog(f"_interpret_singlepada: found {f_pada} not in ")
                res2 = self.a.label_samjna(pseq, f_pada)
                if res2:
                    debuglog("_interpret_singlepada: label_samjna {} returned ".format(f_pada), ppformat(res2), level=1)
                    return res2
            return labelval.intersect(pseq)

        if f_pada in r().get_skip_words():
            self.debuglog("interpret_pada: Skipping ", f_pada)
            return None
        # If f_pada is encountered first time, evaluate and remember it
        if not self.a.eval_samjna(f_pada):
            return None
        res = self.a.label_samjna(pseq, f_pada)
        if res:
            self.debuglog("label_samjna {} returned ".format(f_pada), res, level=1)
            return res
        return None

    # All samasta-padas other than bahupada dvandvam
    def _interpret_conjunct(self, pseq: PadaSequence, f_desc, fpada):
        if fpada.find('=') < 0:
            return self._interpret_singlepada(pseq, f_desc, fpada)

        r = pseq
        negate = False
        for c in fpada.split('='):
            if c == 'a' or c == 'an':
                negate = True
                continue
            r = self._interpret_singlepada(r, f_desc, c)
            if not r:
                break

        if negate:
            debuglog("negating {}".format(c))
            if r:
                return None
            return pseq
        else:
            return r

    # Return output whose format is same as returned by find_label()
    # Process a single or compound sutra pada
    def _interpret_pada(self, pseq: PadaSequence, f_desc):
        fpada = Subanta.praatipadikam(f_desc)
        if not fpada:
            return None
        self.debuglog("_interpret_pada({}): {}".format(fpada, pseq))
        if fpada.find('-') < 0:
            r = self._interpret_singlepada(pseq, f_desc)
            self.debuglog("interpret_pada {}({}) = {}".format(fpada, pseq, r))
            return r if r else None

        if fpada.endswith("Adi"):
            r = pseq
            for c in fpada.split('-'):
                r = self._interpret_singlepada(r, f_desc, c)
                if not r:
                    return r
            return r

        pinds = None
        vinds = None
        matched = False
        negate = False
        r = None
        results = []
        for c in fpada.split('-'):
            if c == 'a' or c == 'an':
                negate = True
                continue
            if c == 'anta' or c == 'Adi':
                r, prevc = results[-1]
                if not r:
                    continue
                r = self._interpret_singlepada(r, f_desc, c)
            else:
                r = self._interpret_conjunct(pseq, f_desc, c)

            self.debuglog("interpret_pada {} = {}".format(c, r))
            results.append((r, c))

            if negate:
                self.debuglog("negating {}".format(c, r), level=1)
                if r:
                    return None
                matched = True
            else:
                if r:
                    debuglog(f"r = {ppformat(r)}")
                    if c in r.labels:
                        debuglog(f"{r.labels[c]}")
                        if 'varnas' in r.labels[c]:
                            inds = reduce(lambda x, y: x + y, [m['pos'] for m in r.labels[c]['varnas']])
                            if not vinds:
                                vinds = []
                            vinds.extend(inds)
                        if 'padas' in r.labels[c]:
                            inds = reduce(lambda x, y: x + y, [m['pos'] for m in r.labels[c]['padas']])
                            if not pinds:
                                pinds = []
                            pinds.extend(inds)
                    else:
                        if 'padas' in r.views:
                            if not pinds:
                                pinds = []
                            pinds.extend(r.views['padas'])
                        if 'varnas' in r.views:
                            if not vinds:
                                vinds = []
                            vinds.extend(r.views['varnas'])
                    matched = True

        r = pseq
        if pinds or vinds:
            self.debuglog(f"pinds={pinds} vinds={vinds}")
            r = pseq.subset(pinds=pinds, vinds=vinds)
        return r if matched else None

    # Return pada_desc if cond applies to it, None otherwise
    def _if(self, pseq, rule):
        cond = rule[0]
        if isinstance(cond, list):
            res = functools.reduce((lambda x, y: x and y),
                (self._interpret_pada(pseq, c) for c in cond))
        else:
            res = self._interpret_pada(pseq, cond)

        self.debuglog("_if = ", res)
        return self._apply_func(pseq, rule[1:]) if res else None

    def _ifnot(self, pseq, rule):
        cond = rule[0]
        if isinstance(cond, list):
            res = functools.reduce((lambda x, y: x and y),
                                   (self._interpret_pada(pseq, c) for c in cond))
        else:
            res = self._interpret_pada(pseq, cond)

        self.debuglog("_ifnot = ", res)
        return self._apply_func(pseq, rule[1:]) if not res else None

    def _find_anga(self, pseq: PadaSequence, pratyaya_desc):
        res = self._interpret_pada(pseq, pratyaya_desc[0])
        if not res:
            return res
        pratyaya_vidhi_i = res.padas(res.views['padas'][0]-1)['pos'][0]
        pinds = []
        debuglog(f"_find_anga: {pratyaya_vidhi_i} res = {ppformat(res)}")
        for i in res.views['padas']:
            pratyaya_i = res.padas(i)['pos'][0]
            pos = list(range(pratyaya_vidhi_i, pratyaya_i))
            res.add_label(".".join(['padas', 'aNga']), positions=[i],
                          value={'type': 'varnas', 'val': pos })
            pinds.append(i)
        debuglog(f"_find_anga: {res.labels['aNga']}")
        return res.subset(pinds=pinds)

    # pseq should contain only one pada
    def _gen_sequence(self, pseq: PadaSequence, seq_desc):
        if len(pseq.varnas()) > 3:
            return None
        self.debuglog("start gen_sequence")
        first = self._apply_func(pseq, seq_desc[0])
        self.debuglog("gen_sequence: first = ", ppformat(first))
        if not first:
            return None
        elif isinstance(first, list):
            first = first[0]
        last = self._apply_func(pseq, seq_desc[1])
        self.debuglog("gen_sequence: last = ", ppformat(first))
        if not last:
            return None
        elif isinstance(last, list):
            last = last[0]
        self.debuglog("genseq: {},{}".format(first, last))
        seq = self.a.gen_sequence(first, last)
        self.debuglog("gen_sequence({}, {}) = {}".format(first, last, seq))
        if not seq:
            return None

        pada = pseq.padas()[0]['pada']
        pseq.add_label(".".join(['padas', pada]), positions=[0],
                       value= { 'type' : 'varnas', 'val' : seq})
        return pseq

    def _locate_first_upadesha(self, pseq, rules):
        return self._locate_upadesha(pseq, rules, False)

    def _locate_last_upadesha(self, pseq, rules):
        return self._locate_upadesha(pseq, rules, True)

    # Apply rules on a pada sequence pseq
    # pseq should contain only one pada
    def _locate_upadesha(self, pseq: PadaSequence, rules, reverse=False):
        res = self._apply_func(pseq, rules[0])
        if not res:
            return None

        varnas = [c for c in res.varnas()]
        self.debuglog("locate_upadesha", varnas)
        matched_upadeshas = self.a.locate_in_upadesha(varnas[0])
        if not matched_upadeshas:
            return None

        mymatches = matched_upadeshas
        if reverse:
            mymatches = reversed(mymatches)
        next_matches = []
        for m in mymatches:
            u_desc = self.a.get_upadesha(m)
            #self.a.upadesha[m['key']][m['idx']].subset(vinds=m['pos'])
            self.debuglog("Checking match of upadesha ...", u_desc)
            matched = True
            for r in rules[1:]:
                if not self._apply_func(u_desc, r):
                    matched = False
                    break
            if matched:
                next_matches.append(m)
                break
        if not next_matches:
            return None
        matched_upadeshas = next_matches
        self.debuglog("locate result: ", matched_upadeshas)
        return matched_upadeshas

    # Check given pada against a generated samjna rule
    def _gen_samjna(self, pseq, func_descs):
        self.debuglog(f"In gen_samjna {pseq}")
        fdesc = func_descs[0]
        self.debuglog(ppformat(fdesc['samjni']), level=2)
        res = self._apply_func(pseq, fdesc['samjna'])
        self.debuglog("gen_samjna returned samjna = ", res)
        if res and fdesc['samjni']:
            res = self._apply_func(res, fdesc['samjni'])
            if res:
                pada = pseq.padas(0)['pada']
                if 'literal' in res.labels:
                    litval = res.labels['literal']['padas'][0]['val']
                    res_pada = res.padas(0)['pada']
                    m = re.fullmatch(r'(.)a', res_pada)
                    if m:
                        pseq.add_label(".".join(['varnas', res_pada]), positions=[0],
                                       value={'type': 'varnas',
                                              'val': [m.group(1)]})
                    else:
                        pseq.add_label(".".join(['padas', pada]), positions=[0],
                                   value = { 'type': 'padas',
                                             'literal': litval,
                                             'val': res_pada})
                else:
                    vlist = [v for v in res.varnas()]
                    pseq.add_label(".".join(['padas', pada]), positions=[0],
                                   value= { 'type' : 'varnas',
                                            'val' : vlist})
            return pseq
        return res

    def _pipe(self, pseq, func_descs):
        res = pseq
        for f in func_descs:
            res = self._apply_func(res, f)
            self.debuglog("pipe component result", res, level=2)
            if not res:
                return None
            #intersect = intersect & set(res) if intersect else set(res)
            #if not intersect:
            #    return None
        self.debuglog("Pipe result: ", res, level=2)
        return res

    # A conjunct of Samjna components
    def _samjna_comps(self, samjna_pseq, samjna_conds):
        res = samjna_pseq
        for c in samjna_conds:
            res = self._apply_func(res, c)
            self.debuglog("samjna component result", res)
            if not res:
                return None
            #intersect = intersect & set(res) if intersect else set(res)
            #if not intersect:
            #    return None
        self.debuglog("Samjna_comps result: ", res)
        return res

    # Split given property list into those matching or not matching the cond. 
    def bifurcate(self, pseq: PadaSequence, cond, split_p6 = False):
        matches = []
        rest = []
        # Attach Shashthi vibhakti padas to nearest non-shashthi pada list
        v6_padas = []
        last_match = None
        for p in pseq:
            if self.a._propmatch(p, cond):
                if v6_padas:
                    matches[0:0] = v6_padas
                    v6_padas = []
                matches.append(p)
                last_match = matches
            else:
                if split_p6 and self.a._propmatch(p, {'vibhakti': 6}):
                    v6_padas.append(p)
                    continue
                if v6_padas:
                    rest[0:0] = v6_padas
                    v6_padas = []
                rest.append(p)
                last_match = rest
        if v6_padas and last_match:
            last_match[0:0] = v6_padas
        return (matches, rest)

    def _compile_samjna(self, plist):
        func_descs = []
        yasmaat, rest = self.bifurcate(plist, {'pada': 'yasmAt'})
        if yasmaat:
            pratyaya, rest = self.bifurcate(rest, {'praatipadikam': 'pratyaya'})
            if pratyaya:
                return ["ANGA"] + pratyaya
            return func_descs

        p7, rest = self.bifurcate(plist, {'vibhakti' : 7})
        if p7:
            func_descs.extend(["IF", p7])

        # Handle generated Samjna names
        match, rest = self.bifurcate(rest, {'praatipadikam' : 'rUpa'}, False)
        if match:
            #print "Entered sva"
            match, rest = self.bifurcate(rest, {'praatipadikam' : 'sva'}, False)
            if match:
                #print "Entered rUpa"
                p6, rest = self.bifurcate(rest, {'vibhakti' : 6})
                samjna = self._compile_samjna(rest) if rest else None
                if p6:
                    func_descs.extend(["GEN_SAMJNA",
                        { 'samjni' : ["PIPE"] + p6, 'samjna' : samjna }])
                else:
                    func_descs.extend(["GEN_SAMJNA",
                        { 'samjni' : None, 'samjna' : samjna }])

                return func_descs

    #        p6, rest = self.bifurcate(rest, {'vibhakti' : 6})
    #        if p6:
    #            func_descs.append(["PIPE"] + p6)

        match, rest = self.bifurcate(rest, {'pada' : 'na', 'vibhakti' : 0})
        if match:
            if rest:
                subrule = self._compile_samjna(rest)
                func_descs.append(["IFNOT", subrule])
                return func_descs

        p1, rest = self.bifurcate(rest, {'vibhakti' : 1})
        match, rest = self.bifurcate(rest, {'pada' : 'saha', 'vibhakti' : 0})
        if match:
            p3, rest = self.bifurcate(rest, {'vibhakti' : 3})
            return ["GEN_SEQ", ["LOCATE_FIRST"] + p1, ["LOCATE_LAST"] + p3]

        if rest:
            p6, rest = self.bifurcate(rest, {'vibhakti': 6})
            if p6:
                p1 = p6 + p1
                #func_descs.extend(["IF", p6])
            else:
                # Handles alo.antyAt pUrva
                p5, rest = self.bifurcate(rest, {'vibhakti': 5})
                if p5:
                    purva_para, rest = self.bifurcate(p1, {'praatipadikam': ['pUrva', 'para']})
                    p1 = p5 + purva_para + rest

        func_descs.extend(["SAMJNA_COMPS"] + p1)
        return func_descs

    def compile_samjna(self, plist):
        plist = [self.a.pada_override(fdesc) for fdesc in plist]
        self.rule = self._compile_samjna(plist)
        return self.rule

    def __repr__(self):
        return self.rule

    def apply(self, pseq):
        res = self._apply_func(pseq, self.rule)
        return res

class Vidhi_rule(Rule):
    def __init__(self, ashtadhyayi, sutra_id, rule = None):
        super(Vidhi_rule, self).__init__(ashtadhyayi, rule)
        self.sutra_id = sutra_id
        self.MODIFY = self._modify
        self.TAG = self._tag

        # Key should be in SLP1 format
        self.predefined_vidhi_funcs = {
            'lopa' : lambda pseq, mods, m: self._lopa(pseq, mods, m),
            #'adarSana' : lambda pseq, loc, ctx, m: self._antya(x),
            'pratyaya': lambda pseq, mods, m: pseq,
            r'para$': lambda pseq, mods, m: pseq,
            r'(.*)savarRa': lambda pseq, mods, m: self._savarna(pseq, mods, m),
            r'dIrGa': lambda pseq, mods, m: self._dIrGa(pseq, mods, m),
        }

    def _dIrGa(self, pseq, mods, m):
        debuglog(f"_dIrGa: {pseq} {ppformat(mods)}")
        modified = False
        for i in mods['inds']:
            if mods['part'] == 'varnas':
                purva_varna = pseq.varnas(i)
                newvarnas = [Siksha.dIrGa(purva_varna)]
                debuglog(f"purva={purva_varna} dIrGa={newvarnas}")
                vinds = [i]
                if mods['replace'] == 2:
                    vinds.append(i+1)
                pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
                modified = True
        return pseq if modified else None

    def _savarna(self, pseq, mods, m):
        debuglog(f"_savarna: {pseq} {ppformat(mods)}")
        modified = False
        for i in mods['inds']:
            if mods['part'] == 'varnas':
                purva_varna = pseq.varnas(i)
                para_varna = pseq.varnas(i+1)
                debuglog(f"purva={purva_varna} para={para_varna}")
                newvarnas = Siksha.savarRa(purva_varna, para_varna)
                pseq.upsertvarnas([i], newvarnas, mods['replace'])
                modified = True
        return pseq if modified else None

    def _lopa(self, pseq, mods, m):
        lopa_positions = mods['inds']
        debuglog("_lopa: lopa_positions = ", lopa_positions)
        pseq.delvarnas(lopa_positions)
        #vinds = [i for i in pseq.views['varnas'] if i not in lopa_positions]
        #self.debuglog("_lopa: vinds = ", vinds, level=2)
        #return pseq.subset(vinds=vinds)
        return pseq

    def _pararUpa(self, pseq, mods, m):
        lopa_positions = mods['inds']
        self.debuglog("_lopa: lopa_positions = ", lopa_positions, level=0)
        pseq.delvarnas(lopa_positions)
        #vinds = [i for i in pseq.views['varnas'] if i not in lopa_positions]
        #self.debuglog("_lopa: vinds = ", vinds, level=2)
        #return pseq.subset(vinds=vinds)
        return pseq

    def _para(self, pseq, func_descs):
        if 'padas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['padas']:
                if pos >= len(pseq.parts['padas'])-1:
                    continue
                res = pseq.subset(pinds=[pos + 1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos+1)
            if matched_pos:
                res = pseq.subset(pinds=matched_pos)
                res.add_label("padas.para", matched_pos)
                self.debuglog(f"_para: padas {ppformat(res)}")
                return res

        if 'varnas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['varnas']:
                if pos >= len(pseq.parts['varnas'])-1:
                    continue
                res = pseq.subset(vinds=[pos+1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos+1)
            if matched_pos:
                res = pseq.subset(vinds=matched_pos)
                res.add_label("varnas.para", matched_pos)
                self.debuglog(f"_para: varnas {ppformat(res)}")
                return res
        return None

    def _purva(self, pseq, func_descs):
        if 'padas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['padas']:
                if pos == 0:
                    continue
                res = pseq.subset(pinds=[pos - 1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos-1)
            if matched_pos:
                res = pseq.subset(vinds=matched_pos)
                res.add_label("padas.purva", matched_pos)
                return res

        if 'varnas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['varnas']:
                if pos == 0:
                    continue
                res = pseq.subset(vinds=[pos-1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos-1)
            if matched_pos:
                res = pseq.subset(vinds=matched_pos)
                res.add_label("varnas.purva", matched_pos)
                return res
        return None

    def _locate(self, pseq, loc, func_descs):
        res = pseq
        if not func_descs:
            return res
        fnames = []
        for f in func_descs:
            self.debuglog(f"_locate {ppformat(f)} {res}")
            res = super(Vidhi_rule, self)._apply_func(res, f)
            if not res:
                return res
            fnames.append(Subanta.praatipadikam(f))

        if res:
            loc['fnames'] = fnames
            for part in ['padas', 'varnas']:
                if part in res.views:
                    loc['pos'] = {
                        'part': part,
                        'inds': res.views[part]
                    }
                    res.add_label(f"{part}.sthaana", positions=res.views[part])
                    loc['result'] = res.views
        return res

    def _modify(self, pseq, func_desc):
        repl_parms = func_desc[0]
        repl_funcs = func_desc[1]
        debuglog("_modify: ", ppformat(func_desc), level=2)
        debuglog("_modify: ", pseq, level=2)
        debuglog("_modify: ", ppformat(repl_parms), level=2)

        if ('purva' in repl_parms) and ('para' not in repl_parms):
            skip = True
            for f in repl_funcs:
                f_pada = Subanta.praatipadikam(f)
                if (f_pada in self.a.upadesha_idx and
                        self.a.upadesha_idx[f_pada] == 'Agama'):
                    log(f"_modify: Skipping due to aagama {f_pada}")
                    skip = False
            if skip:
                return None

        loc = {}
        for location in ['sthaanas', 'para', 'purva']:
            if location in repl_parms:
                loc[location] = {}
                res = self._locate(pseq, loc[location], repl_parms[location])
                if not res:
                    self.debuglog(f"_locate {location}: {ppformat(repl_parms)} {pseq}")
                    del loc[location]
                    return None

        if not loc:
            return None
        self.debuglog(f"_modify: {ppformat(loc)}", level=1)
        mods = {}
        if 'sthaanas' in loc:
            sthaana_loc = loc['sthaanas']
            if 'para' in loc:
                para_loc = loc['para']
                if sthaana_loc['pos']['part'] == para_loc['pos']['part']:
                    inds1 = sthaana_loc['pos']['inds']
                    inds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = sthaana_loc['pos']['part']
                    inds = set(inds1) & set(inds2)
                    mods['para'] = para_loc
                elif sthaana_loc['pos']['part'] == 'padas':
                    inds1 = [pseq.padas(i)['pos'][1]-1 for i in sthaana_loc['pos']['inds']]
                    inds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = 'varnas'
                    inds = set(inds1) & set(inds2)
                    mods['para'] = para_loc
                else:
                    vinds1 = sthaana_loc['pos']['inds'] # varna
                    pinds1 = [i for i, p in enumerate(pseq.parts['padas'])
                                if (set(vinds1) & set(range(*p['pos']))) and
                                    i+1 in para_loc['pos']['inds']]
                    debuglog(f"_modify: vinds_sthana {vinds1} pinds_para {para_loc['pos']['inds']} pinds_sthana {pinds1}")
                    if not pinds1:
                        return None
                    inds = reduce(lambda x,y: x | y, [set(range(*pseq.parts['padas'][i]['pos'])) for i in pinds1])
                    if not inds:
                        return None
                    mods['part'] = sthaana_loc['pos']['part']
                    inds = sthaana_loc['pos']['inds']
            else:
                mods['part'] = sthaana_loc['pos']['part']
                inds = sthaana_loc['pos']['inds']

            mods['replace'] = 2 if 'both' in repl_parms else 1
            mods['purva'] = sthaana_loc
            mods['inds'] = inds
        elif 'para' in loc:
            para_loc = loc['para']
            if 'purva' in loc:
                purva_loc = loc['purva']
                if purva_loc['pos']['part'] == para_loc['pos']['part']:
                    vinds1 = purva_loc['pos']['inds']
                    vinds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = purva_loc['pos']['part']
                elif purva_loc['pos']['part'] == 'padas':
                    vinds1 = [pseq.padas(i)['pos'][1]-1 for i in purva_loc['pos']['inds']]
                    vinds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = 'varnas'
                else:
                    vinds1 = purva_loc['pos']['inds'] # varna
                    vinds2 = [pseq.padas(i)['pos'][0]-1 for i in para_loc['pos']['inds']]
                    mods['part'] = 'varnas'

                vinds = set(vinds1) & set(vinds2)
                mods['replace'] = 2 if 'both' in repl_parms else 0
                mods['purva'] = purva_loc['result']
                mods['para'] = para_loc['result']
                mods['inds'] = vinds
        else: # only purva condition
            purva_loc = loc['purva']
            mods['replace'] = 0
            mods['purva'] = purva_loc['result']
            mods['part'] = purva_loc['pos']['part']
            mods['inds'] = purva_loc['pos']['inds']

        self.debuglog(f"_modify: {ppformat(mods)}", level=2)

        if not mods['inds']:
            return None

        # Save current plist state in transform history
        pseq.push(self.sutra_id)

        self.debuglog("_modify: ", ppformat(repl_funcs), level=2)
        res = pseq
        for f in repl_funcs:
            res = self._transform(res, mods, f)
            if not res:
                break
        if res:
            res.labels = {}
        return res

    # Return output whose format is same as returned by find_label()
    def _transform(self, pseq: PadaSequence, mods, func_desc):
        self.debuglog("_transform({}): {} @ location {}".format(ppformat(func_desc),
                                                                pseq, ppformat(mods)))
        new_fdesc = func_desc
        r = self._transform_one(pseq, mods, new_fdesc)
        self.debuglog("transform_one {}({}) = {}".format(new_fdesc['pada'], pseq, r), level=1)
        return r

    def _yatha_sankhyam(self, pseq, vinds, f_desc, mods):
        srch_pada = "".join([pseq.varnas(i) for i in vinds])
        purva_fpada = mods['purva']['fnames'][-1]
        self.debuglog(f"yatha_sankhyam: purva_pada={purva_fpada} mod_f={f_desc}")

        def expand_fpada(fpada):
            mylist = []
            for c in fpada.split('-'):
                if c in self.a.samjna_defs:
                    if 'members' in self.a.samjna_defs[c]:
#                        print(f"expand_fpada: members of {c}: {self.a.samjna_defs[c]}")
                        val = self.a.samjna_defs[c]['members']['val']
                        if isinstance(val, list):
                            mylist.extend(self.a.samjna_defs[c]['members']['val'])
                        else:
                            mylist.append(val)
                else:
                    mylist.append(c)
#            print(f"expand_fpada: {fpada} {mylist}")
            return mylist

        purva_list = expand_fpada(purva_fpada)
        pos = [i for i,c in enumerate(purva_list) if c == srch_pada]
        repl_list = expand_fpada(f_desc['praatipadikam'])
        self.debuglog(f"yatha_sankhyam: {srch_pada}{pos} {purva_list} -> {repl_list}")

        return repl_list[pos[0]] if pos[0] in range(len(repl_list)) else None

    # f_pada input must be in praatipadika form
    def _transform_one(self, pseq, mods, f_desc):
        if not mods or not mods['inds']:
            return None
        f_pada = Subanta.praatipadikam(f_desc)

        log("_transform_one({}): {}".format(ppformat(f_pada), ppformat(mods)), level=2)
        if re.match(r'^na$', f_pada) and f_desc['vibhakti'] == 0:
            return None
        for f in self.predefined_vidhi_funcs:
            m = re.match(f, f_pada)
            if m:
                log("matched {} against predef {}".format(pseq, f))
                res = self.predefined_vidhi_funcs[f](pseq, mods, m)
                return res

        add_label = None
        m = re.fullmatch(r'(.)a', f_pada)
        if m:
            self.debuglog(f"_transform_one: {f_pada} is akaaranta", level=2)
            f_pada = m.group(1)
        elif f_pada in self.a.upadesha_idx:
            self.debuglog(f"_transform_one: upadesha {f_pada}: checking 'it'")
            if self.a.upadesha_idx[f_pada] == 'Agama':
                if 'para' not in mods:
                    if pseq.find_label(f_pada):
                        return None
                    add_label = f"padas.{f_pada}"
                mods['replace'] = 0
                if f_pada.endswith('w'):
                    mods['inds'] = [i-1 for i in mods['inds']]
            u_desc = PadaSequence(padas=[f_pada])
            res = self.a.label_samjna(u_desc, 'it')
            self.debuglog(f"_transform_one: {f_pada}: ", res, level=2)
            if not res:
                return None

            self.debuglog(f"_transform_one: {f_pada}: Applying sutra 13009 tasya lopaH")
            f_desc, _ = self.a.apply_vidhi_rule(u_desc, sutra_id='13009')  # tasya (itaH) lopaH
            f_pada = "".join(f_desc.varnas())

        self.debuglog("_transform_one not predef ({}): {}".format(f_desc, ppformat(mods)), level=1)
        modified = False
        if mods['part'] == 'padas':
            for i in mods['inds']:
                self.debuglog(f"_transform_one: at pada {i} replace={mods['replace']} pada {f_pada}", level=1)
                if not mods['replace']:
                    pseq.modpada(f_desc, i)
                    modified = True
                elif 'pada_split' in f_desc:
                    vinds = [j for j in range(*pseq.padas(i)['pos'])]
                    newvarnas = self._yatha_sankhyam(pseq, vinds, f_desc, mods)
                    pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
        else:
            for i in mods['inds']:
                vinds = [i]
                if mods['replace'] == 2:
                    vinds.append(i+1)

                if f_pada == 'pararUpa':
                    vinds.pop()
                    newvarnas = None
                elif 'pada_split' in f_desc:
                    newvarnas = self._yatha_sankhyam(pseq, vinds, f_desc, mods)
                elif f_pada in self.a.samjna_defs:
                    defn = self.a.samjna_defs[f_pada]
                    self.debuglog(f"_transform_one({f_pada}: defn = {ppformat(defn)}")
                    if 'members' in defn:
                        val = defn['members']['val']
                        if isinstance(val, str):
                            newvarnas = [val[0]]
                        elif isinstance(val, list):
                            for v in val:
                                newvarnas = Siksha.savarRa(pseq.varnas(i), v)
                                if not newvarnas:
                                    continue
                                self.debuglog(f"{newvarnas} {pseq.varnas(i)} vs {v}")
                    else:
                        newvarnas = []
                        pluta = False
                        for vi in vinds:
                            v = pseq.varnas(vi)
                            pluta = pluta or ('3' in v)
                            savarnas = Siksha.savarRa(v)
                            self.debuglog(f"savarnas({v}) = {savarnas}")
                            if not newvarnas:
                                newvarnas = set(savarnas)
                            newvarnas = set(newvarnas) & set(savarnas)
                            newseq = PadaSequence(padas=newvarnas)
                            newseq = Rule(self.a)._interpret_pada(newseq, f_desc)
                            if not newseq:
                                return None
                            newvarnas = newseq.varnas()
                            self.debuglog(f"newvarnas = {newvarnas}")

                        # Look for common varnas with same kaala and most restrictive sthaana as v1,v2
                        if pluta:
                            # remove dIrgha varnas
                            rmvarnas = [v for v in newvarnas if v in Siksha.v['dIrGa']]
                        else:
                            # remove pluta varnas
                            rmvarnas = [v for v in newvarnas if v in Siksha.v['pluta']]
                        newvarnas = Siksha.max_savarna(set(newvarnas) - set(rmvarnas))
                        if not newvarnas:
                            return None
                else:
                    newvarnas = Pada.split(f_pada, '3\\\^~')
                self.debuglog("_transform_one: ", f_pada, vinds, newvarnas, pseq)
                pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
                if add_label:
                    if mods['replace'] == 0:
                        # if inserting an Agama in between two padas,
                        # Make it logically part of the next pada, not previous.
                        nextind = vinds[0]+len(newvarnas)+1
                        for i,p in enumerate(pseq.parts['padas']):
                            if nextind == p['pos'][0]:
                                pos = list(p['pos'])
                                pos[0] -= len(newvarnas)
                                p['pos'] = pos
                                pos = list(pseq.padas(i-1)['pos'])
                                pos[1] -= len(newvarnas)
                                pseq.padas(i-1)['pos'] = pos
                                break
#                    print(f"Adding {add_label} as label")
                    PadaSequence(pseq.prev_transforms[-1]['pseq']).add_label(add_label, positions=[0], value=True)
                modified = True

        return pseq if modified else None

    def _tag(self, pseq: PadaSequence, func_descs):
        res = pseq
        tags = [Subanta.praatipadikam(f) for f in func_descs[1]]
        matched = False
        tags_found = [t for t in tags if t in pseq.labels]
        log(f"_tag: preexisting tags: {tags_found}")
        if tags_found:
            return None

        for f in func_descs[0]:
            res = super(Vidhi_rule, self)._apply_func(res, f)
            if not res:
                continue
            samjna = Subanta.praatipadikam(f)
            newlabel = copy.deepcopy(res.labels[samjna])
            for t in tags:
                pseq.labels[t] = newlabel
                for part,matches in pseq.labels[t].items():
                    for m in matches:
                        m['sutra_id'] = self.sutra_id
            matched = True

        return pseq if matched else None

    def compile(self, plist):
        plist = [self.a.pada_override(f_desc) for f_desc in plist]
        func_descs = []

        # To handle kartari krt
        p1, rest1 = self.bifurcate(plist, {'vibhakti': 1})
        if p1:
            p7, rest1 = self.bifurcate(rest1, {'vibhakti': 7})
            if p7:
                artha_saptami = True
                for p in p1:
                    if not self.a.is_samjna(Subanta.praatipadikam(p)):
                        artha_saptami = False
                        break
                if artha_saptami and not rest1:
                    #self.debuglog(f"found samjna {p['pada']} and artha-saptami", level=2)
                    self.rule = ["TAG", p1, p7]
                    return self.rule

        repl_parms = {}
        p6, rest = self.bifurcate(plist, {'vibhakti': 6})
        if p6:
            purvapara, rest6 = self.bifurcate(p6, {'praatipadikam' : 'pUrva-para'})
            if purvapara:
                eka, rest = self.bifurcate(rest,
                                           {'praatipadikam' : 'eka',
                                            'vibhakti': 1})
                repl_parms['both'] = True if eka else False
            if rest6:
                repl_parms['sthaanas'] = rest6

        p7, rest = self.bifurcate(rest, {'vibhakti': 7})
        if p7:
            repl_parms['para'] = p7

        p5, rest = self.bifurcate(rest, {'vibhakti': 5})
        if p5:
            repl_parms['purva'] = p5

        p3, rest = self.bifurcate(rest, {'vibhakti': 3})
        if p3:
            if 'purva' not in repl_parms:
                repl_parms['purva'] = []
            repl_parms['purva'].extend(p3)
            if 'para' not in repl_parms:
                repl_parms['para'] = []
            repl_parms['para'].extend(p3)

        p1, rest = self.bifurcate(rest, {'vibhakti': 1})
        if p1:
            match, rest = self.bifurcate(plist, {'pada': 'na', 'vibhakti': 0})
            if match:
                p1 = match + p1
            func_descs = ["MODIFY", repl_parms, p1]

        self.rule = func_descs
        return self.rule

    def apply(self, pseq: PadaSequence):
        return self._apply_func(pseq, self.rule)
