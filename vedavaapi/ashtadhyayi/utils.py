# -*- coding: utf-8 -*-

import locale
import sys
import json
import re
from functools import reduce, wraps
from indic_transliteration import sanscript
from pprint import pprint, pformat
import copy
import inspect
import csv

def dumper(obj):
    try:
        if isinstance(obj, set):
            return list(obj)
        elif callable(obj):
            return obj.__name__ + "()"
        res = obj.__repr__()
        return res
    except Exception as e:
        print(f"dumper: caught exception: {e}")
        ret = obj.__dict__
        print(f"dumper: {ret}")
        return ret

class ReqContext:
    def __init__(self, *, inscript="SLP1", outscript="SLP1", loglevel = 0):
        self.curseq = -1
        self.opstack = []
        self.ops = []
        self.loglevel = loglevel
        self.inscript = inscript
        self.outscript = outscript
        self.subop('main')

    def subop(self, opname, parms=None):
        par_id = self.opstack[-1] if len(self.opstack) > 0 else None
        newop = {'id': len(self.ops),
                 'par_id' : par_id,
                 'op': opname,
                 'parms': parms,
                 'result': None,
                 'skip_words': [],
                 'trace': []}
#        pprint(newop)
        self.ops.append(newop)
        self.opstack.append(newop['id'])
        #print(f"opstack start = {self.opstack}")
        if par_id != None:
            self.ops[par_id]['trace'].append({'id': newop['id']})
        self.curseq = newop['id']

    def skip_word(self, samjna):
        self.curop()['skip_words'].append(samjna)
    def get_skip_words(self):
        skips = [set(self.ops[i]['skip_words']) \
         for i in self.opstack if 'skip_words' in self.ops[i]]
        names = [self.ops[i]['op'] for i in self.opstack]
        debuglog(f"skipped words: {skips}")
        log(f"names: {names}")
        return reduce(lambda x,y: x | y, skips)
    def curop(self):
        return self.ops[self.opstack[-1]]

    def msg(self, msg):
        self.curop()['trace'].append(msg)

    def endop(self, result=None):
        if result:
            self.curop()['result'] = result
        self.opstack.pop()
#        log(f"endop {self.curop()['id']}")
#        if callable(result):
#            log(f"result = {result.__name__}")
#        else:
#            log(f"result = {result}")
        return result

    def expand(self, id):
#        print(f"expanding {id}")
        op = self.ops[id]
        for subop in op['trace']:
            if isinstance(subop, dict) and 'id' in subop:
                try:
                    subobj = self.expand(subop['id'])
                    for k,v in subobj.items():
                        subop[k] = v
                except Exception as e:
                    print(f"Exception in expand: {e}")
                    sys.exit(1)
        return op

    def __repr__(self):
        res = self.expand(0)
        if isinstance(res, list):
            print(res)
        return res

    def __str__(self):
        return self.ops

_reqctx = None
def set_req_context(req):
    global _reqctx
    _reqctx = req

def r():
    global _reqctx
    return _reqctx

def logged_func(f_to_decorate):
    @wraps(f_to_decorate)
    def wrapper(*args, **kwargs):
        #print('The positional arguments are', args)
#        print('The keyword arguments are', kwargs)
        args_name = inspect.getfullargspec(f_to_decorate)[0]
#       print(args_name)

        nargs = dict(zip(args_name, args))
        if 'self' in nargs:
            del nargs['self']
#        print(nargs)
        for k, v in kwargs.items():
#           print(f_to_decorate.__name__, k, v)
            nargs[k] = v
        r().subop(f_to_decorate.__name__, nargs)
        res = f_to_decorate(*args, **kwargs)
        return r().endop(res)

    return wrapper

def utf8_decode(string):
    return string

def print_dict(mydict):
    stext = json.dumps(mydict, default=dumper, indent=2, ensure_ascii=False, separators=(',', ': '))
    return stext

def byteify(input):
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def transcribe(input, modifier):
    if isinstance(input, tuple):
        input = list(input)
    if isinstance(input, dict):
        return dict((key, transcribe(value, modifier)) for key,value in input.items())
    elif isinstance(input, list):
        return [transcribe(element, modifier) for element in input]
    elif isinstance(input, str):
        inp = modifier(input)
        return inp
    else:
        return input

class DotDict(dict):
    def __getattr__(self, name):
        return self[name]

def is_ascii(s: str) -> bool:
    try:
        s.encode('ascii')
        return True
    except:
        return False

_rmchars = [ "\uFEFF", "\u200c", "\u200d", "\u201c", "\u201d" ]
_accents = { "\u0952" : "\\", "\u0951" : "/" }
def xliterate(mystr, from_script = "DEVANAGARI", to_script = "SLP1"):
    from_script = eval("sanscript." + from_script)
    to_script = eval("sanscript." + to_script)
    l = mystr
    l = re.sub(r'[' + ''.join(_rmchars) + ']', '', l)
    for a,aval in _accents.items():
        l = l.replace(a, aval)
        #l = re.sub(r'[' + a + ']', aval, l)
    l = sanscript.transliterate(l, from_script, to_script)
    return l

def xliterate_obj(myobj, from_script = "DEVANAGARI", to_script = "SLP1"):
    mystr = json.dumps(myobj, indent=4, ensure_ascii=False, separators=(',', ': '))
    return json.loads(xliterate(mystr, from_script, to_script))

_loglevel = 1
def loglevel(level):
    r().loglevel = int(level)
    print("Log level set to", r().loglevel)

_outscript = "SLP1"
def script(scr):
    r().outscript = scr
    log("Out language script set to", r().outscript)

def log(*msg, level=1, indent=0, op=None):
    if level <= r().loglevel:
        m = " ".join([str(m) for m in msg])
        #m = xliterate(m, "SLP1", "IAST")
        for l in m.split('\n'):
            txt = "{}: {}{}".format(str(level) + (' ' + op if op else ''), ' ' * int(indent), l)
            r().msg(txt)
            print(txt)

def ppformat(msg, inscr="SLP1", outscr=None):
    outscript = outscr if outscr else r().outscript
    obj = msg
    if inscr != outscript:
        obj = transcribe(obj,
                         lambda s: xliterate(s, from_script=inscr, to_script=outscript))
    return pformat(obj, compact=True)

def debuglog(*msg, level=2, indent=0):
    log(*msg, level=level)

def errlog(*msg, indent=0):
    log(*msg, level=0)

def info(*msg, indent=0):
    log(*msg, level=0)

def read_csvfile(infile):
    try:
        with open(infile, 'r') as f:
            reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True)
            props = None
            for row in reader:
                if not props:
                    props = row
                    continue
                yield dict(zip(props, row))
        return None
    except Exception as e:
        print(f"Error reading {infile}: {e}")
        return None
